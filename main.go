package main

import (
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"time"

	"github.com/ajstarks/svgo"
)

// Position is a goki position on board
type Position struct {
	X     int
	Y     int
	Color string
}

func main() {
	http.Handle("/grid", http.HandlerFunc(grid))
	log.Println(time.Now())
	err := http.ListenAndServe(":2003", nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func grid(w http.ResponseWriter, req *http.Request) {

	positions := []Position{}
	gmax := 19
	xmax := 0
	ymax := 0
	c := ""
	cw := 40

	// generate some random positions
	for p := 0; p < 10; p++ {
		x := rand.Intn(6) + 1
		if x > xmax {
			xmax = x
		}
		y := rand.Intn(6) + 1
		if y > ymax {
			ymax = y
		}
		if p%2 == 0 {
			c = "white"
		} else {
			c = "black"
		}
		positions = append(positions, Position{x, y, c})
	}

	// Create a default grid, based on positions
	s := svg.New(w)
	w.Header().Set("Content-Type", "image/svg+xml")

	gxs := cw
	gxe := xmax * cw
	gys := cw
	gye := ymax * cw

	s.Start(gxe+cw, gye+cw)

	// draw the lines
	for xl := cw; xl <= gxe; xl += cw {
		gym := gmax * cw
		if gym > gye {
			gym = gye + cw
		}
		s.Line(xl, gys, xl, gym, "stroke:grey; stroke-width:1")
	}
	for yl := cw; yl <= gye; yl += cw {
		gxm := gmax * cw
		if gxm > gxe {
			gxm = gxe + cw
		}
		s.Line(gxs, yl, gxm, yl, "stroke:grey; stroke-width:1")
	}

	// draw hoshis
	hoshis := []Position{
		{4, 4, "grey"},
		{4, 10, "grey"},
		{4, 16, "grey"},
		{10, 4, "grey"},
		{10, 10, "grey"},
		{10, 16, "grey"},
		{16, 4, "grey"},
		{16, 10, "grey"},
		{16, 16, "grey"},
	}

	for _, hoshi := range hoshis {
		if hoshi.X <= xmax && hoshi.Y <= ymax {
			s.Circle(
				hoshi.X*cw,
				hoshi.Y*cw,
				cw/10,
				fmt.Sprintf("fill:%s; stroke: black", hoshi.Color),
			)
		}
	}

	for _, position := range positions {
		s.Circle(
			position.X*cw,
			position.Y*cw,
			cw/2-1,
			fmt.Sprintf("fill:%s; stroke: black", position.Color),
		)
	}

	s.End()
}
